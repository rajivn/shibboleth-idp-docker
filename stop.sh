#!/bin/bash -e

function docker_remove_containers {
    local CMD=$( docker ps -a | cut -d ' ' -f 1 | sed -e 's/CONTAINER//' | grep --color=never -E "[a-z0-9]+" )
    for DID in $CMD; do
	docker rm $DID;
    done
}

if [ "$( docker ps -a | grep obscureitylogger )" != "" ]; then
    docker kill obscureitylogger
fi
if [ "$( docker ps -a | grep obscureitycopy )" != "" ]; then
    docker kill obscureitycopy
fi
docker kill obscureity
docker kill shibboleth
docker kill phpldapadmin
docker kill slapd

docker_remove_containers

#!/bin/bash -e

CWD=$( pwd );

cd $(dirname $0 )
WORKDIR=$( pwd );
echo $WORKDIR

cd $WORKDIR/slapd
echo "Building slapd"
docker build -t slapd:latest .
echo "Done"
echo

cd $WORKDIR/phpldapadmin
echo "Building phpldapamdin"
docker build -t phpldapadmin:latest .
echo "Done"
echo

cd $WORKDIR/obscureity
echo "Building obscureity"
docker build -t obscureity:latest .
echo "Done"
echo

cd $CWD

BUILDING
========

Run `build.sh`. This should build containers named `slapd:latest`, `phpldapadmin:latest` and `obscureity:latest`


RUNNNING
========

Run `start.sh`. Runs the 3 containers in daemon mode. If you want to look at logs in the shibboleth container,
just run `start.sh` with the option `--with-logger`

Once the docker containers are running, you can authenticate via the LDAP backend using the following credentials:

`Username` - shibboleth

`Password` - 123


STOPPING
========

Run `stop.sh`.

#!/bin/bash -e

echo -n "Creating a volume container ... "
docker create -v /opt/shibboleth-idp --name shibboleth obscureity:latest /bin/true
echo "Created"

echo -n "Starting slapd Server ... "
docker run -d -p 8389:389 --name slapd slapd:latest
echo " Started"

echo -n "Starting phpldapadmin Server ... "
docker run -d -p 8300:80 --link slapd:slapd --name phpldapadmin phpldapadmin:latest
echo " Started"

echo -n "Starting Obscureity ... "
docker run -d -p 8080:8080 --volumes-from shibboleth --link slapd:slapd --name obscureity obscureity:latest
echo "Started"

echo -n "Starting Obscureity Copy ... "
docker run -d -p 8081:8080 --link slapd:slapd --name obscureitycopy obscureity:latest
echo "Started"

if [ "$1" == "--with-logger" ]; then
    TIMEOUT=60
    echo "Waiting for $TIMEOUT seconds before tailing log. It takes a long time for jetty to start :("
    sleep $TIMEOUT
    echo -n "Starting logger ... "
    docker run -it --volumes-from shibboleth --name obscureitylogger obscureity:latest tail -f /opt/shibboleth-idp/logs/idp-process.log
    echo "Started"
fi
